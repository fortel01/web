#!/bin/sh

URL="$RANCHER_API_URL/project/$RANCHER_PROJECT_ID/workloads/$RANCHER_WORKSPACE_ID"

WS_INFO=$(curl -s $URL \
               -H "Authorization: Bearer $RANCHER_API_TOKEN" \
               -H 'Connection: keep-alive' \
               -H 'Pragma: no-cache' \
               -H 'Cache-Control: no-cache' 2>&1)

WS_REDEPLOY=$(echo $WS_INFO | sed "s/\"cattle\.io\/timestamp\"\:\"[0-9T:Z-]*\"/\"cattle\.io\/timestamp\":\"$(date -u +"%Y-%m-%dT%H:%M:%SZ")\"/g")

curl $URL -X PUT -H "Authorization: Bearer $RANCHER_API_TOKEN" \
                 -H 'Connection: keep-alive' \
                 -H 'Pragma: no-cache' \
                 -H 'Cache-Control: no-cache' \
                 -H 'content-type: application/json' \
                 -H 'accept: application/json' \
                 -d "$WS_REDEPLOY"
